# OpenML dataset: pair0001

https://www.openml.org/d/43279

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

//Add the description.md of the data file pair0001

Information for pairs0001:

DWD data (Deutscher Wetterdienst)

data was taken at 349 stations

taken from
//*http://www.dwd.de/bvbw/appmanager/bvbw/dwdwwwDesktop/?_nfpb=true&_pageLabel=_dwdwww_klima_umwelt_klimadat//en_deutschland&T82002gsbDocumentPath=Navigation%2FOeffentlichkeit%2FKlima__Umwelt%2FKlimadaten%2Fkldaten__//kostenfrei%2Fausgabe__mittelwerte__node.html__nnn%3Dtrue*

more recent  link (Jan 2010):

//*http://www.dwd.de/bvbw/appmanager/bvbw/dwdwwwDesktop/?_nfpb=true&_pageLabel=_dwdwww_klima_umwelt_klimadat//en_deutschland&T82002gsbDocumentPath=Navigation%2FOeffentlichkeit%2FKlima__Umwelt%2FKlimadaten%2Fkldaten__//kostenfrei%2Fausgabe__mittelwerte__node.html__nnn%3Dtrue*


x: altitude

y: temperature (average over 1961-1990)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43279) of an [OpenML dataset](https://www.openml.org/d/43279). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43279/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43279/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43279/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

